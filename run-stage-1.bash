#!/bin/bash
#
#SBATCH --job-name=run_experiments-stage-1
#SBATCH --partition=all
#
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=30
#
#SBATCH --array=0-17

srun python3 -m src.parameterOptimization stage-1 $SLURM_ARRAY_TASK_ID 0 30