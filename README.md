For a high-level overview over the code and what does what refer [to the architecture file](/ARCHITECTURE.md).

# Goals

Goals include:
* Extensibility of simulation - so that e.g. testing, vaccinations or more complex policies can be retrofitted easily.
* Fast - so that learning of policy parameters goes fast.
* Understandable - to make collaboration with others easier and maybe make the simulation more useful for them.

Non-goals are:
* Realism - we use an abstract model on purpose to better be able to model different small communities and scenarios.
* COVID realism - we model an abstract epidemic disease.


# Executing stuff
* Graph generation ```poetry run python -m src.graphgen```
* Simulation ```poetry run python -m src.simulation iterations``` or ```poetry run python -m src.simulation fitness```
* Stats after simulation run ```poetry run python -m src.stats```
* Learning a simple parameter problem ```poetry run python -m src.parameterOptimization debug 0 4 3```

# Helpers for nicer code
* ```poetry run black src```
* ```poetry run mypy src```
* ```poetry run autoflake -r --in-place src```

# Profiling
* ```poetry run python -m cProfile -o profile.stats -m src.simulation```
* For interpreting the resulting file: ```poetry run python -m snakeviz profile.stats```. This get's you a graphical explorer for the data.