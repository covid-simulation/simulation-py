import glob
from multiprocessing import Pool, cpu_count
from timeit import default_timer as timer

from src.stats.helpers import (
    enrich_dataframe,
    peak,
    peak_infected,
    cumulative as cumulative_mapping,
)
from src.types import IterationRunSpec
import matplotlib.pyplot as plt
import pandas as pd
import logging

logger = logging.getLogger("STATS")


def stat_file_to_dataframe(file: str):
    return pd.read_csv(
        file,
        dtype={
            "susceptible": int,
            "exposed": int,
            "infected": int,
            "dead": int,
            "recovered": int,
        },
    )


def read_stat_files(run: IterationRunSpec, p):
    folder = run.get_folder_helper()
    files = glob.glob(f"{folder}/iteration-*/stats.csv")

    dfs = p.map(stat_file_to_dataframe, files)

    dfs = list(map(enrich_dataframe, dfs))
    dfs.sort(key=peak)
    return dfs


def location_file_to_dataframe(file: str):
    return pd.read_csv(
        file,
        dtype={"Person": str, "location": str},
    )


def group(d):
    return d.groupby("location").size()


def read_and_prepare_location_file(run: IterationRunSpec, type: str, p):
    base_folder = run.get_folder_helper()
    folders = sorted(glob.glob(f"{base_folder}/iteration-*"))
    files = list(
        map(lambda f: sorted(glob.glob(f"{f}/{type}-location-*.csv")), folders)
    )  # is list of lists

    dfs = []
    for idx in range(len(files)):
        fs = files[idx]

        locs = p.map(location_file_to_dataframe, fs)
        dfs.append(list(p.map(group, locs)))
        if idx % 10 == 0 and idx > 1:
            logger.warning(f"Loaded {idx} location file sets")

    return dfs


def merge_location_data_to_daily(ld):
    chunks = [ld[i : i + 7] for i in range(0, len(ld), 7)]

    processed_chunks = []
    for c in chunks:
        data = c[0]
        for i in range(1, len(c)):
            data = data.add(c[i], fill_value=0)
        processed_chunks.append(data)
    return processed_chunks


def aggregate_by_location_type(iteration_day):
    def agg(elem):
        if elem.startswith("flat"):
            return "Flats"
        if elem.startswith("lecture"):
            return "Lectures"
        if elem.startswith("sport"):
            return "Sport courses"
        return "Cafeterias"

    return iteration_day.groupby(by=agg).sum()


def grouped_sum(df):
    return df.groupby(by=lambda x: x).sum().fillna(0)


def plot_merged_location_data(dataframe, run):
    folder = run.get_folder_helper()

    plt.pie(
        dataframe["total_infection_chances"], labels=dataframe.index, autopct="%1.1f%%"
    )
    plt.savefig(f"{folder}/merged_total_infection_chances_pie.png")
    plt.clf()

    plt.pie(
        dataframe["total_infection_chances_fixed"],
        labels=dataframe.index,
        autopct="%1.1f%%",
    )
    plt.savefig(f"{folder}/merged_total_infection_chances_fixed_pie.png")
    plt.clf()

    plt.pie(
        dataframe[("infected_fixed", "mean")], labels=dataframe.index, autopct="%1.1f%%"
    )
    plt.savefig(f"{folder}/merged_infections_fixed_pie.png")
    plt.clf()

    plt.pie(
        dataframe[("susceptible_fixed", "mean")],
        labels=dataframe.index,
        autopct="%1.1f%%",
    )
    plt.savefig(f"{folder}/merged_susceptible_fixed_pie.png")
    plt.clf()

    # BAR CHARTS
    plt.bar(
        dataframe.index,
        dataframe[("infected_fixed", "mean")],
        yerr=dataframe[("infected_fixed", "std")],
        capsize=5,
    )
    plt.ylabel("Infected visiting", fontsize=24)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.tight_layout()
    plt.savefig(f"{folder}/merged_infections_fixed_bar.png")
    plt.clf()

    plt.bar(
        dataframe.index,
        dataframe[("susceptible_fixed", "mean")],
        yerr=dataframe[("susceptible_fixed", "std")],
        capsize=5,
    )
    plt.ylabel("Susceptible visiting", fontsize=24)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.tight_layout()
    plt.savefig(f"{folder}/merged_susceptible_fixed_bar.png")
    plt.clf()

    plt.bar(dataframe.index, dataframe["total_infection_chances_fixed"], capsize=5)
    plt.ylabel("Infection chances", fontsize=24)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.tight_layout()
    plt.savefig(f"{folder}/merged_total_infection_chances_fixed_bar.png")
    plt.clf()

    plt.bar(dataframe.index, dataframe["total_infection_chances"], capsize=5)
    plt.ylabel("Infection chances", fontsize=24)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.tight_layout()
    plt.savefig(f"{folder}/merged_total_infection_chances_bar.png")
    plt.clf()


def make_histogram(csv_file, values, plot_file):
    with open(csv_file, "w") as file:
        file.write("\n".join(map(str, values)))

    plt.hist(values[("infected_fixed", "mean")])
    plt.xlabel("Number of infected", fontsize=24)
    plt.ylabel("Days in group", fontsize=24)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.tight_layout()
    plt.savefig(plot_file.format("infected"))
    plt.clf()

    plt.hist(values[("susceptible_fixed", "mean")])
    plt.xlabel("Number of susceptible", fontsize=24)
    plt.ylabel("Days in group", fontsize=24)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.tight_layout()
    plt.savefig(plot_file.format("susceptible"))
    plt.clf()


def histograms_location_data(merged, run):
    foods = merged.filter(regex="food-.*", axis="index")
    flats = merged.filter(regex="flat-.*", axis="index")
    lectures = merged.filter(regex="lecture-.*", axis="index")
    sports = merged.filter(regex="sport-.*", axis="index")

    folder = run.get_folder_helper()

    make_histogram(
        f"{folder}/location-food.csv", foods, f"{folder}/hist-location-food-{'{}'}.png"
    )
    make_histogram(
        f"{folder}/location-flat.csv", flats, f"{folder}/hist-location-flat-{'{}'}.png"
    )
    make_histogram(
        f"{folder}/location-lecture.csv",
        lectures,
        f"{folder}/hist-location-lecture-{'{}'}.png",
    )
    make_histogram(
        f"{folder}/location-sport.csv",
        sports,
        f"{folder}/hist-location-sport-{'{}'}.png",
    )


def analyze_location_data(run, dfs, infected, susceptible, p):
    infected_by_day = list(p.map(merge_location_data_to_daily, infected))
    susceptible_by_day = list(p.map(merge_location_data_to_daily, susceptible))

    data = []
    data_by_location_type = []
    for i in range(len(infected_by_day)):
        tmp = []
        for j in range(len(infected_by_day[i])):
            inf = infected_by_day[i][j].rename("infected")
            sus = susceptible_by_day[i][j].rename("susceptible")
            df = inf.to_frame().join(sus.to_frame(), how="outer").fillna(0)
            df["susceptible_fixed"] = df[df["infected"] > 0]["susceptible"]
            df["infected_fixed"] = df[df["susceptible"] > 0]["infected"]
            tmp.append(df.fillna(0))
        data.append(tmp)
        data_by_location_type.append(p.map(aggregate_by_location_type, tmp))

    # data contains [iterations][day] -> df with locations

    merged = p.map(pd.concat, data)
    merged = list(p.map(grouped_sum, merged))
    merged = pd.concat(merged).groupby(by=lambda x: x).agg(["mean", "std"]).fillna(0)
    merged.columns = merged.columns.to_flat_index()

    merged_by_location = p.map(pd.concat, data_by_location_type)
    merged_by_location = list(p.map(grouped_sum, merged_by_location))
    merged_by_location = (
        pd.concat(merged_by_location)
        .groupby(by=lambda x: x)
        .agg(["mean", "std"])
        .fillna(0)
    )
    merged_by_location.columns = merged_by_location.columns.to_flat_index()

    # total infection chances
    merged["total_infection_chances"] = (
        merged[("susceptible", "mean")] * merged[("infected", "mean")]
    )
    merged["total_infection_chances_fixed"] = (
        merged[("susceptible_fixed", "mean")] * merged[("infected_fixed", "mean")]
    )
    merged_by_location["total_infection_chances"] = (
        merged_by_location[("susceptible", "mean")]
        * merged_by_location[("infected", "mean")]
    )
    merged_by_location["total_infection_chances_fixed"] = (
        merged_by_location[("susceptible_fixed", "mean")]
        * merged_by_location[("infected_fixed", "mean")]
    )

    # logger.warning(merged)
    # logger.warning(merged_by_location)

    folder = run.get_folder_helper()
    merged.to_csv(f"{folder}/processed_locations.csv")
    merged_by_location.to_csv(f"{folder}/processed_locations_types.csv")

    plot_merged_location_data(merged_by_location, run)
    histograms_location_data(merged, run)


def make_run_histogram(data, filename: str, xlabel: str):
    plt.hist(data)
    # plt.title("Infection Peak")
    plt.xlabel(xlabel, fontsize=24)
    plt.ylabel("Runs in group", fontsize=24)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.tight_layout()
    plt.savefig(filename)
    plt.clf()


def aggregate_runs_histogram_fitness_values(run: IterationRunSpec, dfs):
    folder = run.get_folder_helper()
    peaks = list(map(peak, dfs))
    infected_peaks = list(map(peak_infected, dfs))
    cumulative = list(map(cumulative_mapping, dfs))

    with open(f"{folder}/infected-peaks.csv", "w") as file:
        file.write("\n".join(map(str, infected_peaks)))
    # make_run_histogram(infected_peaks, f"{folder}/hist_infection-peak.png", "$f_{peak}$ groups")

    with open(f"{folder}/total-peaks.csv", "w") as file:
        file.write("\n".join(map(str, peaks)))
    make_run_histogram(peaks, f"{folder}/hist_total-infection-peak.png", "$f_{peak}$")

    with open(f"{folder}/cumulative.csv", "w") as file:
        file.write("\n".join(map(str, cumulative)))
    make_run_histogram(
        cumulative, f"{folder}/hist_total-infections.png", "$f_{cumulative}$"
    )


def make_quartile_plot(label: str, filename: str, key: str, q1_df, median_df, q3_df):
    plt.plot(q1_df.index, q1_df[key], label="25%-Quartile")
    plt.plot(median_df.index, median_df[key], label="50%-Quartile")
    plt.plot(q3_df.index, q3_df[key], label="75%-Quartile")
    plt.xlabel("t [days]", fontsize=24)
    plt.ylabel(label, fontsize=24)
    plt.legend(prop={"size": 16})
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.tight_layout()
    plt.savefig(filename)
    plt.clf()


def aggregate_median_by_peak(run, dfs):
    folder = run.get_folder_helper()
    peaks = list(map(peak, dfs))

    n = len(peaks)
    q1 = int((n + 1) / 4)
    median = int((n + 1) / 2)
    q3 = int((n + 1) * 3 / 4)

    q1_df = dfs[q1]
    median_df = dfs[median]
    q3_df = dfs[q3]

    make_quartile_plot(
        "Total infections",
        f"{folder}/curves_total-infections.png",
        "total_infected",
        q1_df,
        median_df,
        q3_df,
    )
    make_quartile_plot(
        "Active infections",
        f"{folder}/curves_infections.png",
        "infected",
        q1_df,
        median_df,
        q3_df,
    )
    make_quartile_plot(
        "$cost_t$", f"{folder}/curves_cost.png", "cost", q1_df, median_df, q3_df
    )


def post_process(run: IterationRunSpec):
    start = timer()

    with Pool(cpu_count()) as p:
        logger.warning("Reading stats to dataframes...")
        dfs = read_stat_files(run, p)
        logger.warning("Aggregating histogram data...")
        aggregate_runs_histogram_fitness_values(run, dfs)
        logger.warning("Aggregating median runs by peak...")
        aggregate_median_by_peak(run, dfs)

        mid = timer()
        logger.warning(f"Time first part: {mid - start} s")

        logger.warning("Aggregate infected location data...")
        infected = read_and_prepare_location_file(run, "infected", p)

        logger.warning("Aggregate susceptible location data...")
        susceptible = read_and_prepare_location_file(run, "susceptible", p)

        mid2 = timer()
        logger.warning(f"Time first part: {mid2 - mid} s")

        logger.warning("Analyze location data...")
        analyze_location_data(run, dfs, infected, susceptible, p)

    end = timer()
    logger.warning(f"Time first part: {end - mid2} s")
