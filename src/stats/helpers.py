def enrich_dataframe(df):
    df["total_infected"] = df["exposed"] + df["infected"]
    df["cumulative"] = df["total_infected"] + df["dead"] + df["recovered"]
    return df


def peak(df):
    return df["total_infected"].max()


def peak_infected(df):
    return df["infected"].max()


def cumulative(df):
    return df["cumulative"].max()
