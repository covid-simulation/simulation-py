import sys
import logging
from src.experiments import get_simulation_runs
from src.stats.postprocessing import post_process

logger = logging.getLogger("STATS")


if __name__ == "__main__":

    if sys.argv[1] == "postprocessing":
        list = sys.argv[2]
        idx = int(sys.argv[3])
        experiment = get_simulation_runs(list, idx)
        post_process(experiment)
    elif sys.argv[1] == "postprocessing-list":
        list = sys.argv[2]
        idx = int(sys.argv[3])
        for i in range(idx):
            logger.warning(f"\nPostprocessing experiment {i}")
            experiment = get_simulation_runs(list, i)
            post_process(experiment)
    else:
        logger.error("Please pass 'postprocessing' and correct arguments execution!")
