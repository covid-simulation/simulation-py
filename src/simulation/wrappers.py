import functools
import os
import statistics
from typing import List, Dict, Tuple
from timeit import default_timer as timer
import pandas as pd
from multiprocessing import Pool, cpu_count, Manager

from cachetools import cached

from src.graphgen import load_graph
from src.stats.helpers import enrich_dataframe, peak
from src.simulation.simulation import simulate, write_stats
import logging

from src.types import GraphSpec

logger = logging.getLogger("SIMULATION")
logging.basicConfig(level=logging.INFO)


def simulate_single_iteration(
    spec: GraphSpec,
    max_lecture_size: int,
    max_food_visits: int,
    max_sport_size: int,
    folder: str = "",
    iteration: int = 0,
    write_stats_to_disk: bool = True,
    enable_advanced_stats: bool = True,
):
    start_time = timer()

    if enable_advanced_stats:
        base_folder = (
            spec.get_run_stats_iteration_folder(iteration)
            if folder.strip() == ""
            else folder + f"/iteration-{iteration}"
        )
        os.makedirs(base_folder, exist_ok=True)

    stats: List[Dict[str, int]] = []
    logger.info(f"Starting iteration {iteration + 1}")
    people, locations, visits = load_graph(spec)
    logger.info(
        f"\tLoaded graph with {len(people)} people, {len(locations)} locations and {len(visits)} visits"
    )
    simulate(
        people,
        locations,
        visits,
        stats,
        spec,
        max_lecture_size,
        max_food_visits,
        max_sport_size,
        iteration,
        enable_advanced_stats=enable_advanced_stats,
        folder=folder,
    )
    stats_df = pd.DataFrame(
        stats,
        columns=["susceptible", "exposed", "infected", "dead", "recovered", "cost"],
    )
    stats_df = enrich_dataframe(stats_df)
    if write_stats_to_disk:
        write_stats(iteration, spec, stats_df, folder)
    end_time = timer()
    logger.info(f"Time for iteration: {end_time - start_time}")
    return stats_df


def evaluate_fitness(
    spec: GraphSpec, max_lecture_size: int, max_food_visits: int, max_sport_size: int
) -> Tuple[int, int, int]:
    """
    Returns the fitness of a policy set as tuple of (peak, cumulative, cost)
    """
    stats = simulate_single_iteration(
        spec,
        max_lecture_size=max_lecture_size,
        max_food_visits=max_food_visits,
        max_sport_size=max_sport_size,
        iteration=1,
        write_stats_to_disk=False,
        enable_advanced_stats=False,
    )
    return peak(stats), stats["cumulative"].max(), stats["cost"].mean()


# Note: The dict must be shared, else every process does its own caching...
manager = Manager()
cache = manager.dict()
lock = manager.Lock()


@cached(cache=cache, lock=lock)
def evaluate_fitness_thorough(
    spec: GraphSpec, max_lecture_size: int, max_food_visits: int, max_sport_size: int
) -> Tuple[float, float, float]:
    logger.warning(f"[CACHE SIZE] {len(cache)}")
    return evaluate_fitness_median(3)(
        spec, max_lecture_size, max_food_visits, max_sport_size
    )


@cached(cache=cache, lock=lock)
def evaluate_fitness_extremely_thorough(
    spec: GraphSpec, max_lecture_size: int, max_food_visits: int, max_sport_size: int
) -> Tuple[float, float, float]:
    logger.warning(f"[CACHE SIZE] {len(cache)}")
    return evaluate_fitness_median(7)(
        spec, max_lecture_size, max_food_visits, max_sport_size
    )


def evaluate_fitness_median(iterations: int):
    """
    Returns the fitness of a policy set as tuple of (peak, cumulative, cost).
    To avoid fluctations run several times and pick median.

    Currently picking median for every measure. Might it make more sense to pick the median peak and all three measures of that?
    """

    def fitness_median_helper(
        spec: GraphSpec,
        max_lecture_size: int,
        max_food_visits: int,
        max_sport_size: int,
    ) -> Tuple[float, float, float]:
        runs = []
        for i in range(iterations):
            runs.append(
                evaluate_fitness(
                    spec, max_lecture_size, max_food_visits, max_sport_size
                )
            )

        peak = statistics.median(map(lambda r: r[0], runs))
        cumulative = statistics.median(map(lambda r: r[1], runs))
        cost = statistics.median(map(lambda r: r[2], runs))

        return peak, cumulative, cost

    return fitness_median_helper


def evaluate_fitness_median_parallel(iterations: int):
    """
    Returns the fitness of a policy set as tuple of (peak, cumulative, cost).
    To avoid fluctations run several times and pick median.

    Currently picking median for every measure. Might it make more sense to pick the median peak and all three measures of that?
    """

    def fitness_median_helper(
        spec: GraphSpec,
        max_lecture_size: int,
        max_food_visits: int,
        max_sport_size: int,
    ) -> Tuple[float, float, float]:
        f = functools.partial(
            evaluate_fitness, spec, max_lecture_size, max_food_visits, max_sport_size
        )
        with Pool(min(cpu_count(), iterations)) as p:
            future_results = [p.apply_async(f) for _ in range(iterations)]
            runs = [f.get() for f in future_results]

        peak = statistics.median(map(lambda r: r[0], runs))
        cumulative = statistics.median(map(lambda r: r[1], runs))
        cost = statistics.median(map(lambda r: r[2], runs))

        return peak, cumulative, cost

    return fitness_median_helper


def simulate_iterations(
    n: int,
    spec: GraphSpec,
    max_lecture_size: int,
    max_food_visits: int,
    max_sport_size: int,
    folder: str = "",
):
    logger.info(
        f"Running simulation with seed {spec.seed} on graph with scaling {spec.graph_scaling} for {n} iterations..."
    )
    with Pool(cpu_count()) as p:
        seeded_simulation = functools.partial(
            simulate_single_iteration,
            spec,
            max_lecture_size,
            max_food_visits,
            max_sport_size,
            folder,
        )
        p.map(seeded_simulation, range(n))
