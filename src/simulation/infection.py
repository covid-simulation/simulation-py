import random
from typing import Tuple, Any, Dict
import pandas as pd
from src.constants import (
    GLOBAL_INFECTION_RATE,
    SYMPTOM_RATE,
    BLOCKS,
    RANDOM_MEETING_RATE,
    FOOD_BLOCKS,
    FOOD_DAYS,
    COST_QUARANTINED_FLAT,
    COST_QUARANTINED_LECTURE,
    DAYS,
    get_block_index,
)
import logging

from src.types import GraphSpec

logger = logging.getLogger("INFECTION")
logging.basicConfig(level=logging.WARNING)


def get_location_grouping(
    active_visits, people, week, day, block, precalculated_data: Dict[str, Any]
):
    # Next find out how many infected people visited each location
    infected_people = people[people["state"] == "I"]
    visiting_infected = pd.merge(
        infected_people, active_visits, left_index=True, right_index=True
    )

    if precalculated_data["enable_advanced_stats"]:
        spec: GraphSpec = precalculated_data["graph_spec"]
        iteration: int = precalculated_data["iteration"]
        folder: str = precalculated_data["folder"]
        base_folder = (
            spec.get_run_stats_iteration_folder(iteration)
            if folder.strip() == ""
            else folder + f"/iteration-{iteration}"
        )
        path = f"{base_folder}/infected-location-w{week}-d{DAYS.index(day)}-b{get_block_index(block)}.csv"
        visiting_infected["location"].to_csv(path, index=True, index_label="Person")

    grouping_by_location = visiting_infected.groupby("location").agg(
        {"inverse_infection_rate": ["prod"]}
    )

    # Removes the prod part of inverse_infection_rate -> prod.
    grouping_by_location.columns = grouping_by_location.columns.droplevel(1)

    return grouping_by_location


def get_potentially_exposed_people(
    active_visits,
    grouping_by_location,
    people,
    week: int,
    day: str,
    block: str,
    precalculated_data: Dict[str, Any],
):
    # Now we can roll the dice for every susceptible person who visits such a location.
    susceptible_people = people[people["state"] == "S"]
    # Visits to locations with infected
    relevant_visits = pd.merge(
        grouping_by_location,
        active_visits[["location"]],
        left_index=True,
        right_on="location",
    )

    potentially_exposed_people = pd.merge(
        susceptible_people, relevant_visits, left_index=True, right_index=True
    )

    if precalculated_data["enable_advanced_stats"]:
        spec: GraphSpec = precalculated_data["graph_spec"]
        iteration: int = precalculated_data["iteration"]
        folder: str = precalculated_data["folder"]
        base_folder = (
            spec.get_run_stats_iteration_folder(iteration)
            if folder.strip() == ""
            else folder + f"/iteration-{iteration}"
        )
        path = f"{base_folder}/susceptible-location-w{week}-d{DAYS.index(day)}-b{get_block_index(block)}.csv"
        potentially_exposed_people["location"].to_csv(
            path, index=True, index_label="Person"
        )

    return potentially_exposed_people["inverse_infection_rate"]


def get_names_of_new_infected(
    active_visits,
    grouping_by_location,
    people,
    week: int,
    day: str,
    block: int,
    precalculated_data: Dict[str, Any],
):
    potentially_exposed_people = get_potentially_exposed_people(
        active_visits,
        grouping_by_location,
        people,
        week,
        day,
        block,
        precalculated_data,
    ).to_frame()

    # Compute the chance of someone not getting infected via (1-Infection Rate)^(meetings).
    # Then roll for getting more that that and set the new infection column.
    potentially_exposed_people["got_infected"] = potentially_exposed_people[
        "inverse_infection_rate"
    ].apply(lambda r: r > random.random())
    names_of_newly_infected = potentially_exposed_people[
        potentially_exposed_people["got_infected"] == True
    ].index
    return names_of_newly_infected


def set_status_for_new_infected(names_of_newly_infected, people):
    if len(names_of_newly_infected) > 0:
        people.loc[
            names_of_newly_infected,
            "state",
        ] = "E"
        # Also roll if people develop symptoms here
        people.loc[
            filter(lambda _: random.random() > SYMPTOM_RATE, names_of_newly_infected),
            "symptoms",
        ] = True


def policy_food_limit(
    visits, precalculated_data: Dict[str, Any], max_food_visits
) -> Tuple[Any, int]:
    food_places = precalculated_data["food_places"]
    food_place_visits = visits[visits["location"].isin(food_places)]

    grouped = food_place_visits.groupby("location")
    sampled = grouped.sample(min(len(grouped), max_food_visits))
    sampled_index = pd.MultiIndex.from_arrays([sampled.index, sampled["location"]])

    visits_index = pd.MultiIndex.from_arrays([visits.index, visits["location"]])
    food_visits_index = pd.MultiIndex.from_arrays(
        [food_place_visits.index, food_place_visits["location"]]
    )
    # we block all food visits that have not been sampled
    blocked_visits = food_place_visits[~food_visits_index.isin(sampled_index)]
    blocked_index = pd.MultiIndex.from_arrays(
        [blocked_visits.index, blocked_visits["location"]]
    )

    cost = food_place_visits[food_visits_index.isin(blocked_index)]["cost"].sum()
    active_visits = visits[~visits_index.isin(blocked_index)]

    logger.debug(f"\t\t\tActive visits after food limit: {len(active_visits)}")

    return active_visits, cost


def limit_policy(
    visits, precalculated_data: Dict[str, Any], location_type: str
) -> Tuple[Any, int]:
    not_allowed = precalculated_data[f"banned_{location_type}"]

    blocked_visits = visits[visits["location"].isin(not_allowed)]
    cost = blocked_visits["cost"].sum()

    active_visits = visits[~visits["location"].isin(not_allowed)]
    logger.debug(
        f"\t\t\tActive visits after {location_type} limit: {len(active_visits)}"
    )

    return active_visits, cost


def policy_lecture_limit(visits, precalculated_data: Dict[str, Any]) -> Tuple[Any, int]:
    return limit_policy(visits, precalculated_data, "lecture")


def policy_sport_limit(visits, precalculated_data: Dict[str, Any]) -> Tuple[Any, int]:
    return limit_policy(visits, precalculated_data, "sport")


def quarantine_flatmates(visits, blocked_visits) -> Tuple[Any, int]:
    quarantined_flats = blocked_visits[
        blocked_visits["location"].str.startswith("flat-")
    ]["location"]
    quarantined_flats = quarantined_flats[
        ~quarantined_flats.duplicated()
    ]  # Drop duplicates does not work on slice.

    active_visits = visits[~visits["location"].isin(quarantined_flats)]
    logger.debug(f"\t\t\tActive visits after quarantine of flats: {len(active_visits)}")
    return active_visits, len(quarantined_flats) * COST_QUARANTINED_FLAT


# Block a lecture if the lecturer is sick
def quarantine_lectures(
    visits, blocked_visits, precalculated_data: Dict[str, Any]
) -> Tuple[Any, int]:
    lecturers = precalculated_data["lecturer_names"]

    lecturer_visits = blocked_visits[blocked_visits.index.isin(lecturers)]
    canceled_lectures = lecturer_visits[
        lecturer_visits["location"].str.startswith("lecture")
    ]["location"]

    active_visits = visits[~visits["location"].isin(canceled_lectures)]
    logger.debug(
        f"\t\t\tActive visits after quarantine of lectures: {len(active_visits)}"
    )

    return active_visits, len(canceled_lectures) * COST_QUARANTINED_LECTURE


def policy_people_with_symptoms(
    active_visits, people, precalculated_data: Dict[str, Any]
) -> Tuple[Any, int]:
    names_of_people_with_symptoms = people[people["symptoms"] == True].index

    blocked_visits = active_visits[
        active_visits.index.isin(names_of_people_with_symptoms)
    ]

    # There originally was a cost += here, but that utterly breaks learning.
    # This is because the sum of symptom disabled visits is incredibly large,
    # thus making a complete lockdown the unilaterally best choice.
    # active_visits[active_visits["person"].isin(names_of_people_with_symptoms)][
    #     "cost"
    # ].sum()
    active_visits = active_visits[
        ~active_visits.index.isin(names_of_people_with_symptoms)
    ]
    logger.debug(f"\t\t\tActive visits after symptoms: {len(active_visits)}")

    active_visits, quarantined_flat_cost = quarantine_flatmates(
        active_visits, blocked_visits
    )

    active_visits, quarantined_lecture_cost = quarantine_lectures(
        active_visits, blocked_visits, precalculated_data
    )

    return active_visits, quarantined_flat_cost + quarantined_lecture_cost


def get_active_visits_under_policies(
    day,
    block,
    visits,
    people,
    max_food_visits: int,
    precalculated_data: Dict[str, Any],
) -> Tuple[Any, int]:
    active_visits = visits[visits["block"] == block]
    logger.debug(f"\t\t\tActive visits: {len(active_visits)}")

    active_visits, lecture_cost = policy_lecture_limit(
        active_visits, precalculated_data
    )

    active_visits, sport_cost = policy_sport_limit(active_visits, precalculated_data)

    # Make sure to only check if we have a time where there is food intake. Otherwise boom.
    # Checking for empty groups before sampling would be much more work.
    food_cost = 0
    if day in FOOD_DAYS and block in FOOD_BLOCKS:
        active_visits, food_cost = policy_food_limit(
            active_visits, precalculated_data, max_food_visits
        )

    active_visits, quarantine_cost = policy_people_with_symptoms(
        active_visits, people, precalculated_data
    )

    logger.debug(f"\t\t\tActive visits after everything: {len(active_visits)}")

    return active_visits, lecture_cost + food_cost + quarantine_cost + sport_cost


def run_infection(
    people,
    visits,
    day,
    week: int,
    max_food_visits: int,
    precalculated_data: Dict[str, Any],
) -> int:
    cost = 0

    if len(people[people["state"] == "I"]) <= 0:
        return cost

    # This needs doing only once, which saves a lot of time, since there are a lot of visits.
    daily_visits = visits[visits["day"] == day]

    for block in BLOCKS:
        active_visits, current_cost = get_active_visits_under_policies(
            day,
            block,
            daily_visits,
            people,
            max_food_visits,
            precalculated_data,
        )
        cost += current_cost
        grouping_by_location = get_location_grouping(
            active_visits, people, week, day, block, precalculated_data
        )
        names_of_newly_infected = get_names_of_new_infected(
            active_visits,
            grouping_by_location,
            people,
            week,
            day,
            block,
            precalculated_data,
        )
        set_status_for_new_infected(names_of_newly_infected, people)

    return cost


def run_global_infection(people):
    total_people = len(people)
    total_infected = len(people[people["state"] == "I"])
    total_susceptible = len(people[people["state"] == "S"])
    # susceptible * infected rate * random meeting rate
    expected_meetings = (
        RANDOM_MEETING_RATE * total_infected * total_susceptible / total_people
    )
    logger.debug(f"\t\t\tGlobal expected meetings: {expected_meetings}")
    new_global_infections = round(expected_meetings * GLOBAL_INFECTION_RATE)
    logger.debug(f"\t\t\tNew global infections: {new_global_infections}")
    set_status_for_new_infected(
        people[people["state"] == "S"].sample(new_global_infections).index, people
    )
