import logging
import sys

from src.experiments import get_simulation_runs
from src.simulation import evaluate_fitness, simulate_iterations
from timeit import default_timer as timer
import pandas as pd

from src.types import GraphSpec

logger = logging.getLogger("SIMULATION")
logging.basicConfig(level=logging.INFO)

if __name__ == "__main__":
    cmd = sys.argv[1]
    logger.warning(f"Running command {cmd}...")
    if cmd == "iterations":
        start = timer()
        simulate_iterations(200, GraphSpec.get_medium_graph(0), 17, 138, 4)
        end = timer()
        logger.warning(f"TOTAL TIME: {end - start}")
    elif cmd == "fitness":
        times = []
        for i in range(200):
            start = timer()
            logger.warning(
                evaluate_fitness(
                    GraphSpec(seed=0, graph_scaling=1, cap_scaling=1), 21, 474, 4
                )
            )
            end = timer()
            times.append(end - start)

        series = pd.Series(times)
        logger.warning(f"Time: {series.mean()} (+/- {series.std()})")
    elif cmd == "experiment":
        experiment = get_simulation_runs(sys.argv[2], int(sys.argv[3]))
        simulate_iterations(
            experiment.iterations,
            experiment.graph,
            experiment.max_lecture,
            experiment.max_food_place,
            experiment.max_sport,
            experiment.get_folder_helper(),
        )
    elif cmd == "experiment-list":
        rnge = range(int(sys.argv[3]))
        list = sys.argv[2]
        for idx in rnge:
            logger.warning(f"Starting experiment {idx}...")
            experiment = get_simulation_runs(list, idx)
            simulate_iterations(
                experiment.iterations,
                experiment.graph,
                experiment.max_lecture,
                experiment.max_food_place,
                experiment.max_sport,
                experiment.get_folder_helper(),
            )
    else:
        logger.error(
            "Please pass 'iterations', 'fitness' or 'experiment' as argument for execution!"
        )
