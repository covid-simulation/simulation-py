import os
import random
import logging

from src.constants import (
    DAYS,
    INITIAL_INFECTIONS,
    MAX_WEEKS,
    EXPOSED_TO_INFECTED_CHANCE,
    INFECTED_TO_DEAD_CHANCE,
    INFECTED_TO_RECOVERED_CHANCE,
    MAX_LECTURE_ATTENDEES,
    MAX_SPORT_ATTENDEES,
)
from src.simulation.infection import run_infection, run_global_infection
from src.types import GraphSpec

logger = logging.getLogger("SIMULATION")
logging.basicConfig(level=logging.WARNING)


def initial_infection(people):
    sampled = people.sample(INITIAL_INFECTIONS)
    people.loc[sampled.index, "state"] = "E"
    logger.debug(f"Infected people {people[people['state'] == 'E']}")


def exposed_to_infected(people):
    people["state"] = people["state"].apply(
        lambda s: "I"
        if s == "E" and random.random() < EXPOSED_TO_INFECTED_CHANCE
        else s
    )


def infected_to_dead(people):
    people["state"] = people["state"].apply(
        lambda s: "D" if s == "I" and random.random() < INFECTED_TO_DEAD_CHANCE else s
    )


def infected_to_recovered(people):
    people["state"] = people["state"].apply(
        lambda s: "R"
        if s == "I" and random.random() < INFECTED_TO_RECOVERED_CHANCE
        else s
    )


def should_terminate(people) -> bool:
    infected = len(people[people["state"] == "I"])
    exposed = len(people[people["state"] == "E"])
    return infected + exposed <= 0


def gather_stats(people, stats, cost=0):
    s = len(people[people["state"] == "S"])
    e = len(people[people["state"] == "E"])
    i = len(people[people["state"] == "I"])
    d = len(people[people["state"] == "D"])
    r = len(people[people["state"] == "R"])
    stats.append(
        {
            "susceptible": s,
            "exposed": e,
            "infected": i,
            "dead": d,
            "recovered": r,
            "cost": cost,
        }
    )
    logger.info(f"\t\t\tS={s}|E={e}|I={i}|D={d}|R={r}|Cost={cost}")


def write_stats(iteration: int, spec: GraphSpec, stats, folder: str = ""):
    folder = (
        spec.get_run_stats_iteration_folder(iteration)
        if folder.strip() == ""
        else folder + f"/iteration-{iteration}"
    )
    os.makedirs(folder, exist_ok=True)
    with open(f"{folder}/stats.csv", "w") as csv_file:
        csv_file.write(stats.to_csv())


def simulate(
    people,
    location,
    visits,
    stats,
    spec: GraphSpec,
    max_lecture_size: int = MAX_LECTURE_ATTENDEES + 1,
    max_food_visits: int = 100000,
    max_sport_size: int = MAX_SPORT_ATTENDEES,
    iteration: int = 0,
    enable_advanced_stats: bool = False,
    folder: str = "",
):
    """Takes the respective dataframes from load_graph"""
    initial_infection(people)
    logger.info("\tInitial infections done.")

    gather_stats(people, stats)

    # Precalculated data
    lecturer_names = people[people["type"] == "lecturer"].index
    lectures = location[location["type"] == "lecture"]
    sports = location[location["type"] == "sport"]
    food_places = location[location["type"] == "food-place"].index
    not_allowed_lectures = lectures[lectures["degree"] > max_lecture_size].index
    not_allowed_sport = sports[sports["degree"] > max_sport_size].index

    precalculated_data = {
        "lecturer_names": lecturer_names,
        "banned_lecture": not_allowed_lectures,
        "banned_sport": not_allowed_sport,
        "food_places": food_places,
        "graph_spec": spec,
        "iteration": iteration,
        "enable_advanced_stats": enable_advanced_stats,
        "folder": folder,
    }

    for week in range(MAX_WEEKS):
        logger.info(f"\t Starting week {week + 1}")
        for day in DAYS:
            logger.info(f"\t\t Starting day {day}")

            exposed_to_infected(people)
            cost = run_infection(
                people,
                visits,
                day,
                week,
                max_food_visits,
                precalculated_data,
            )
            run_global_infection(people)
            infected_to_dead(people)
            infected_to_recovered(people)
            gather_stats(people, stats, cost)
            if should_terminate(people):
                return
