import sys
from typing import List

from pathlib import Path
import logging

logger = logging.getLogger("NORMALIZE")
logging.basicConfig(level=logging.INFO)


def read_solutions(filename: str) -> List[List[float]]:
    """Reads a reference front from a file.
    :param filename: File path where the front is located.
    """
    points = []

    if Path(filename).is_file():
        with open(filename) as file:
            for line in file:
                vector = [float(x) for x in line.split()]

                points.append(vector)
    else:
        logger.warning("Reference front file was not found at {}".format(filename))

    return points


def normalize_points(pts: List[List[float]]) -> List[List[float]]:

    if len(pts[0]) == 2:
        max_infections = max(map(lambda pt: pt[0], pts))
        max_cost = max(map(lambda pt: pt[1], pts))

        return list(map(lambda p: [p[0] / max_infections, p[1] / max_cost], pts))
    elif len(pts[0]) == 3:
        max_peak = max(map(lambda pt: pt[0], pts))
        max_infections = max(map(lambda pt: pt[1], pts))
        max_cost = max(map(lambda pt: pt[2], pts))

        return list(
            map(
                lambda p: [p[0] / max_peak, p[1] / max_infections, p[2] / max_cost], pts
            )
        )
    else:
        logger.error(f"Not able to handle {len(pts[0])} dimensional points!")
        return []


if __name__ == "__main__":
    # Dominated points are correctly ignored
    front = normalize_points(read_solutions(sys.argv[1]))

    # print(front)

    with open(f"aggregate/normalized.txt", "w") as file:
        file.write("\n".join(map(lambda r: f"{' '.join(map(str, r))}", front)))
