from src.types import ExperimentSpec, GraphSpec


# Viable indices are from range(18) (exclusive)
def get_experiment(idx: int) -> ExperimentSpec:
    # Focus on the middle graph for initial exploration of the simulation space
    small_graph = GraphSpec.get_small_graph(0)
    med_graph = GraphSpec.get_medium_graph(0)
    large_graph = GraphSpec.get_large_graph(0)
    population_size = 100
    generations = 100
    low_sample_rate = 1
    mid_sample_rate = 7
    high_sample_rate = 23

    algo = "nsga3"
    learning_iterations = 3

    experiments = [
        # ---------SMALL GRAPH---------------
        ExperimentSpec(
            graph=small_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=low_sample_rate,
            objectives="cumulative",
        ),
        ExperimentSpec(
            graph=small_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=low_sample_rate,
            objectives="peak",
        ),
        ExperimentSpec(
            graph=small_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=low_sample_rate,
            objectives="both",
        ),
        ExperimentSpec(
            graph=small_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=mid_sample_rate,
            objectives="cumulative",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=small_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=mid_sample_rate,
            objectives="peak",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=small_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=mid_sample_rate,
            objectives="both",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=small_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=high_sample_rate,
            objectives="cumulative",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=small_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=high_sample_rate,
            objectives="peak",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=small_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=high_sample_rate,
            objectives="both",
            iterations=learning_iterations,
        ),
        # ---------MEDIUM GRAPH---------------
        ExperimentSpec(
            graph=med_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=mid_sample_rate,
            objectives="cumulative",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=med_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=mid_sample_rate,
            objectives="peak",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=med_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=mid_sample_rate,
            objectives="both",
            iterations=learning_iterations,
        ),
        # ---------LARGE GRAPH---------------
        ExperimentSpec(
            graph=large_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=low_sample_rate,
            objectives="cumulative",
        ),
        ExperimentSpec(
            graph=large_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=low_sample_rate,
            objectives="peak",
        ),
        ExperimentSpec(
            graph=large_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=low_sample_rate,
            objectives="both",
        ),
        ExperimentSpec(
            graph=large_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=mid_sample_rate,
            objectives="cumulative",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=large_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=mid_sample_rate,
            objectives="peak",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=large_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=mid_sample_rate,
            objectives="both",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=large_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=high_sample_rate,
            objectives="cumulative",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=large_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=high_sample_rate,
            objectives="peak",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=large_graph,
            population_size=population_size,
            num_generations=generations,
            algorithm=algo,
            iteration_per_fitness_evaluation=high_sample_rate,
            objectives="both",
            iterations=learning_iterations,
        ),
    ]

    return experiments[idx]
