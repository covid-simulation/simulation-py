from src.types import IterationRunSpec, GraphSpec


# Viable indices are from range(18) (exclusive)
def get_experiment(idx: int) -> IterationRunSpec:
    # Focus on the middle graph for initial exploration of the simulation space
    small_graph = GraphSpec.get_small_graph(0)
    med_graph = GraphSpec.get_medium_graph(0)
    large_graph = GraphSpec.get_large_graph(0)

    experiments = [
        # ---------SMALL GRAPH---------------
        IterationRunSpec(
            graph=small_graph,
            max_food_place=20,
            max_lecture=10,
            max_sport=5,
            prefix="debug/debug-0",
            iterations=20,
        ),
        IterationRunSpec(
            graph=small_graph,
            max_food_place=40,
            max_lecture=40,
            max_sport=25,
            prefix="debug/debug-1",
            iterations=30,
        ),
        IterationRunSpec(
            graph=small_graph,
            max_food_place=40,
            max_lecture=40,
            max_sport=25,
            prefix="debug/debug-2",
            iterations=79,
        ),
        IterationRunSpec(
            graph=large_graph,
            max_food_place=40,
            max_lecture=40,
            max_sport=25,
            prefix="debug/debug-3",
            iterations=79,
        ),
        # HISTOGRAM STUFF
        IterationRunSpec(
            graph=large_graph,
            max_food_place=21,
            max_lecture=474,
            max_sport=4,
            prefix="histogram",
            iterations=199,
        ),
    ]

    return experiments[idx]
