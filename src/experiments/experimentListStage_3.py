from src.types import IterationRunSpec, GraphSpec


# Viable indices are from range(18) (exclusive)
def get_experiment(idx: int) -> IterationRunSpec:
    large_graph = GraphSpec.get_large_graph(0)

    iterations = 199

    experiments = [
        IterationRunSpec(
            graph=large_graph,
            max_lecture=101,
            max_food_place=500,
            max_sport=40,
            prefix="stage-3/no-lockdown",
            iterations=iterations,
        ),
        IterationRunSpec(
            graph=large_graph,
            max_lecture=0,
            max_food_place=0,
            max_sport=0,
            prefix="stage-3/total-lockdown",
            iterations=iterations,
        ),
        IterationRunSpec(
            graph=large_graph,
            max_lecture=5,
            max_food_place=1,
            max_sport=5,
            prefix="stage-3/least-cumulative",
            iterations=iterations,
        ),
        IterationRunSpec(
            graph=large_graph,
            max_lecture=101,
            max_food_place=219,
            max_sport=33,
            prefix="stage-3/most-cumulative",
            iterations=iterations,
        ),
        IterationRunSpec(
            graph=large_graph,
            max_lecture=12,
            max_food_place=1,
            max_sport=4,
            prefix="stage-3/left-of-gap",
            iterations=iterations,
        ),
        IterationRunSpec(
            graph=large_graph,
            max_lecture=20,
            max_food_place=0,
            max_sport=9,
            prefix="stage-3/right-of-gap",
            iterations=iterations,
        ),
        IterationRunSpec(
            graph=large_graph,
            max_lecture=101,
            max_food_place=0,
            max_sport=4,
            prefix="stage-3/only-lectures",
            iterations=iterations,
        ),
        IterationRunSpec(
            graph=large_graph,
            max_lecture=45,
            max_food_place=408,
            max_sport=10,
            prefix="stage-3/low-peak-curve",
            iterations=iterations,
        ),
        IterationRunSpec(
            graph=large_graph,
            max_lecture=61,
            max_food_place=284,
            max_sport=14,
            prefix="stage-3/mid-peak-curve",
            iterations=iterations,
        ),
        IterationRunSpec(
            graph=large_graph,
            max_lecture=66,
            max_food_place=257,
            max_sport=21,
            prefix="stage-3/high-peak-curve",
            iterations=iterations,
        ),
        IterationRunSpec(
            graph=large_graph,
            max_lecture=101,
            max_food_place=418,
            max_sport=34,
            prefix="stage-3/highest-peak",
            iterations=iterations,
        ),
        IterationRunSpec(
            graph=large_graph,
            max_lecture=0,
            max_food_place=500,
            max_sport=40,
            prefix="stage-3/no-lectures",
            iterations=iterations,
        ),
    ]

    return experiments[idx]
