from src.experiments.experimentListStage_1 import get_experiment as get_experiment_1
from src.experiments.experimentListStage_2 import get_experiment as get_experiment_2
from src.experiments.experimentListDebug import get_experiment as get_experiment_debug
from src.experiments.experimentListStage_3_debug import (
    get_experiment as get_run_simulation_debug,
)
from src.experiments.experimentListStage_3 import get_experiment as get_experiment_3
from src.types import ExperimentSpec, IterationRunSpec


def get_experiment(list: str, idx: int) -> ExperimentSpec:
    lists = {
        "debug": get_experiment_debug,
        "stage-1": get_experiment_1,
        "stage-2": get_experiment_2,
    }

    experiment_getter = lists[list]
    if experiment_getter is None:
        raise Exception(f"Wrong experiment list, please select one of {lists.keys()}")

    return experiment_getter(idx)


def get_simulation_runs(list: str, idx: int) -> IterationRunSpec:
    lists = {"debug": get_run_simulation_debug, "stage-3": get_experiment_3}

    experiment_getter = lists[list]
    if experiment_getter is None:
        raise Exception(f"Wrong experiment list, please select one of {lists.keys()}")

    return experiment_getter(idx)
