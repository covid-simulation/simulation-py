from src.types import ExperimentSpec, GraphSpec


# Viable indices are from range(18) (exclusive)
def get_experiment(idx: int) -> ExperimentSpec:
    # Focus on the middle graph for initial exploration of the simulation space
    spec = GraphSpec.get_small_graph(0)
    population_size = 20
    generations = 2
    low_sample_rate = 1
    high_sample_rate = 5

    learning_iterations = 2

    experiments = [
        # ---------NSGA 2---------------
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="nsga2",
            iteration_per_fitness_evaluation=low_sample_rate,
            objectives="cumulative",
        ),
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="nsga2",
            iteration_per_fitness_evaluation=low_sample_rate,
            objectives="peak",
        ),
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="nsga2",
            iteration_per_fitness_evaluation=low_sample_rate,
            objectives="both",
        ),
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="nsga2",
            iteration_per_fitness_evaluation=high_sample_rate,
            objectives="cumulative",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="nsga2",
            iteration_per_fitness_evaluation=high_sample_rate,
            objectives="peak",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="nsga2",
            iteration_per_fitness_evaluation=high_sample_rate,
            objectives="both",
            iterations=learning_iterations,
        ),
        # ---------NSGA 3---------------
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="nsga3",
            iteration_per_fitness_evaluation=low_sample_rate,
            objectives="cumulative",
        ),
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="nsga3",
            iteration_per_fitness_evaluation=low_sample_rate,
            objectives="peak",
        ),
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="nsga3",
            iteration_per_fitness_evaluation=low_sample_rate,
            objectives="both",
        ),
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="nsga3",
            iteration_per_fitness_evaluation=high_sample_rate,
            objectives="cumulative",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="nsga3",
            iteration_per_fitness_evaluation=high_sample_rate,
            objectives="peak",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="nsga3",
            iteration_per_fitness_evaluation=high_sample_rate,
            objectives="both",
            iterations=learning_iterations,
        ),
        # ---------MOEA/D---------------
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="moead",
            iteration_per_fitness_evaluation=low_sample_rate,
            objectives="cumulative",
        ),
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="moead",
            iteration_per_fitness_evaluation=low_sample_rate,
            objectives="peak",
        ),
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="moead",
            iteration_per_fitness_evaluation=low_sample_rate,
            objectives="both",
        ),
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="moead",
            iteration_per_fitness_evaluation=high_sample_rate,
            objectives="cumulative",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="moead",
            iteration_per_fitness_evaluation=high_sample_rate,
            objectives="peak",
            iterations=learning_iterations,
        ),
        ExperimentSpec(
            graph=spec,
            population_size=population_size,
            num_generations=generations,
            algorithm="moead",
            iteration_per_fitness_evaluation=high_sample_rate,
            objectives="both",
            iterations=learning_iterations,
        ),
    ]

    return experiments[idx]
