import sys
from typing import List
from pathlib import Path

import logging

logger = logging.getLogger("PLOTTING")
logging.basicConfig(level=logging.INFO)


def read_solutions(filename: str) -> List[List[float]]:
    """Reads a reference front from a file.
    :param filename: File path where the front is located.
    """
    x = []
    y = []
    z = []

    if Path(filename).is_file():
        with open(filename) as file:
            for line in file:
                vector = [float(x) for x in line.split()]

                x.append(vector[0])
                y.append(vector[1])
                if len(vector) == 3:
                    z.append(vector[2])
    else:
        logger.warning("Reference front file was not found at {}".format(filename))

    if len(z) <= 0:
        return [x, y]
    else:
        return [x, y, z]


def plot_front_to_file(fronts):
    from matplotlib import pyplot as plt

    fig = plt.figure()
    point_size = 25

    if len(fronts[0]) == 3:

        ax = fig.add_subplot(1, 1, 1, projection="3d")

        ax.set_xlabel("$k_{lecture}$", fontsize=24)
        ax.set_ylabel("\n$k_{food}$", fontsize=24)
        ax.set_zlabel("\n$k_{sport}$", fontsize=24)

        # ax.tick_params(axis="x", pad=3)
        # ax.tick_params(axis="y", pad=3)
        ax.tick_params(axis="z", pad=7)

        for label in ax.get_xticklabels() + ax.get_yticklabels() + ax.get_zticklabels():
            label.set_fontsize(12)

        markers = ["s", "x", "o"]
        axes = []

        for idx in range(len(fronts)):
            axes.append(
                ax.scatter(
                    fronts[idx][0],
                    fronts[idx][1],
                    fronts[idx][2],
                    s=point_size,
                    marker=markers[idx],
                )
            )

        # plt.legend(axes, ("1,500 people", "5,000 people", "10,000 people"), prop={"size": 16})
        plt.tight_layout()

    else:
        ax = fig.add_subplot(1, 1, 1)

        ax.set_xlabel("$f_{peak}$", fontsize=24)
        ax.set_ylabel("$f_{cumulative}$", fontsize=24)

        for label in ax.get_xticklabels() + ax.get_yticklabels():
            label.set_fontsize(14)

        markers = ["s", "x", "o"]
        axes = []

        for idx in range(len(fronts)):
            logger.warning(markers[idx])
            axes.append(
                ax.scatter(
                    fronts[idx][0], fronts[idx][1], s=point_size, marker=markers[idx]
                )
            )

        # plt.legend(axes, ("1,500 people", "5,000 people", "10,000 people"), prop={"size": 16}, loc='lower left')
        plt.tight_layout()

    plt.savefig("plots/comparison.png", dpi=1000)


if __name__ == "__main__":
    fronts = [read_solutions(f) for f in sys.argv[1:]]

    plot_front_to_file(fronts)
