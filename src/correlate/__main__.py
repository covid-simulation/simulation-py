import sys

import pandas as pd
import logging

logger = logging.getLogger("CORRELATION")
logging.basicConfig(level=logging.INFO)

MAX_PEOPLE = 5000  # Used for cumulative infections and peak
MAX_COST = 25000  # Guesswork for 5000 people


def read_solutions(filename: str):
    solutions = pd.read_csv(
        filename,
        delim_whitespace=True,
        names=["peak", "cum", "both"],
        dtype={
            "peak": float,
            "cum": float,
            "cost": float,
        },
    )
    return solutions


if __name__ == "__main__":
    # Dominated points are correctly ignored
    front = read_solutions(sys.argv[1])
    corr = front.corr(method="spearman")

    logger.warning(front)
    logger.warning(front.dtypes)
    logger.warning(corr)
