import logging
import sys
from pathlib import Path
from typing import List

logger = logging.getLogger("AGGREGATE")
logging.basicConfig(level=logging.INFO)


def flatten(t):
    # https://stackoverflow.com/questions/952914/how-to-make-a-flat-list-out-of-a-list-of-lists
    return [item for sublist in t for item in sublist]


def read_solutions(filename: str) -> List[List[float]]:
    """Reads a reference front from a file.
    :param filename: File path where the front is located.
    """
    points = []

    if Path(filename).is_file():
        with open(filename) as file:
            for line in file:
                vector = [float(x) for x in line.split()]

                points.append(vector)
    else:
        logger.warning("Reference front file was not found at {}".format(filename))

    return points


def dominates(test: List[float], i: List[float]) -> bool:
    strict_better = False
    # Returns true if test dominates i
    for idx in range(len(test)):
        if test[idx] > i[idx]:
            return False
        if test[idx] < i[idx]:
            strict_better = True
    return strict_better


def kick_out_dominated(
    front: List[List[float]], individuals: List[List[float]]
) -> (List[List[float]], List[List[float]]):

    nondom = []
    with_individuals = individuals != []
    living_individuals = []

    for idx in range(len(front)):
        i = front[idx]
        is_nondom = True
        for test in front:
            if i == test:
                continue
            if dominates(test, i):
                is_nondom = False
                break
        if is_nondom:
            nondom.append(i)
            if with_individuals:
                living_individuals.append(individuals[idx])

    return nondom, living_individuals


if __name__ == "__main__":
    with_search_space = sys.argv[1] == "--searchspace"

    files = sys.argv[1:] if not with_search_space else sys.argv[2:]

    front = flatten(read_solutions(f) for f in files)
    individuals = (
        flatten(read_solutions(f.replace("FUN", "VAR")) for f in files)
        if with_search_space
        else []
    )

    new_front, living_ind = kick_out_dominated(front, individuals)

    with open("aggregate/aggregated.PARAM", "w") as f:
        for i in new_front:
            s = ""
            for val in i:
                s += f"{val} "
            f.write(f"{s.strip()}\n")
    with open("aggregate/VAR.aggregated.PARAM", "w") as f:
        for i in living_ind:
            s = ""
            for val in i:
                s += f"{val} "
            f.write(f"{s.strip()}\n")
