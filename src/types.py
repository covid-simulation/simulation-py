import sys
from typing import NamedTuple

from src.constants import (
    NUM_LECTURERS,
    NUM_COURSES,
    NUM_SPORTS,
    NUM_FOOD_PLACES,
    MAX_LECTURE_ATTENDEES,
    MIN_LECTURE_ATTENDEES,
    MAX_SPORT_ATTENDEES,
    MIN_SPORT_ATTENDEES,
    NUM_STUDENTS,
)


class GraphSpec(NamedTuple):
    """
    This includes the parameters that decide a reproducible graph.
    """

    seed: int
    # Takes the population and makes it smaller by multiplication.
    # Relevant values are NUM_COURSES, NUM_STUDENTS, NUM_LECTURERS, NUM_SPORTS, total people number.
    # This allow simple generation of smaller sets.
    # Please do not set this larger than 1.0!
    graph_scaling: float
    # Similar to graph scaling, but for MAX_{}_ATTENDEES
    cap_scaling: float

    def get_path(self):
        return f"graphs/size-{round(self.graph_scaling*10000)}-cap-{round(self.cap_scaling*100)}/{self.seed}"

    def get_people_path(self):
        return f"{self.get_path()}/people.csv"

    def get_location_path(self):
        return f"{self.get_path()}/locations.csv"

    def get_visits_path(self):
        return f"{self.get_path()}/visits.csv"

    def get_stats_path(self):
        return f"{self.get_path()}/stats.txt"

    def get_run_stats_folder(self):
        return f"run-stats/size-{round(self.graph_scaling*10000)}-cap-{round(self.cap_scaling*100)}/{self.seed}"

    def get_run_stats_iteration_folder(self, iteration: int):
        return f"{self.get_run_stats_folder()}/iteration-{iteration}"

    def get_num_students_adjusted(self):
        return round(NUM_STUDENTS * self.graph_scaling)

    def get_num_lecturers_adjusted(self):
        return round(NUM_LECTURERS * self.graph_scaling)

    def get_num_coursess_adjusted(self):
        return round(NUM_COURSES * self.graph_scaling)

    def get_num_sports_adjusted(self):
        return round(NUM_SPORTS * self.graph_scaling)

    def get_num_food_places_adjusted(self):
        return max(1, round(NUM_FOOD_PLACES * self.graph_scaling))

    def get_max_lecture_attendees_adjusted(self):
        return max(
            round(MAX_LECTURE_ATTENDEES * self.cap_scaling), MIN_LECTURE_ATTENDEES
        )

    def get_max_sport_attendees_adjusted(self):
        return max(round(MAX_SPORT_ATTENDEES * self.cap_scaling), MIN_SPORT_ATTENDEES)

    @staticmethod
    def get_small_graph(seed: int):
        return GraphSpec(seed=seed, graph_scaling=0.15, cap_scaling=0.5)

    @staticmethod
    def get_medium_graph(seed: int):
        return GraphSpec(seed=seed, graph_scaling=0.5, cap_scaling=0.75)

    @staticmethod
    def get_large_graph(seed: int):
        return GraphSpec(seed=seed, graph_scaling=1, cap_scaling=1)


class ExperimentSpec(NamedTuple):
    population_size: int
    num_generations: int
    iteration_per_fitness_evaluation: int
    algorithm: str  # Literal["nsga2", "nsga3", "moead"]
    objectives: str  # Literal["cumulative", "peak", "both"]
    graph: GraphSpec
    # How many times to run the whole experiment
    iterations: int = 1

    def get_num_evaluations(self):
        return self.num_generations * self.population_size

    def get_learning_folder(self, current_iteration: int):
        return f"learning-results/experiment-list-{sys.argv[1]}/{self.algorithm}/{self.objectives}/size-{round(self.graph.graph_scaling * 10000)}-cap-{round(self.graph.cap_scaling * 100)}/p-{self.population_size}-g-{self.num_generations}/fitness-{self.iteration_per_fitness_evaluation}/seed-{self.graph.seed}/iteration-{current_iteration}"

    def get_learning_path(self, filename: str, current_iteration: int):
        return f"{self.get_learning_folder(current_iteration)}/{filename}"

    def get_learning_front_folder(self, current_iteration: int):
        return f"{self.get_learning_folder(current_iteration)}/fronts"

    def get_learning_plot_folder(self, current_iteration: int):
        return f"{self.get_learning_folder(current_iteration)}/plots"


class IterationRunSpec(NamedTuple):
    graph: GraphSpec
    max_food_place: int
    max_lecture: int
    max_sport: int
    prefix: str
    iterations: int

    def get_folder_helper(self):
        parts = self.graph.get_run_stats_iteration_folder(0).split("/")
        return f"{parts[0]}/{self.prefix}/{'/'.join(parts[1:-2])}"
