import logging
import os
import sys
from math import sqrt

from jmetal.algorithm.multiobjective import NSGAII, MOEAD
from jmetal.algorithm.multiobjective.nsgaiii import (
    NSGAIII,
    UniformReferenceDirectionFactory,
)
from src.experiments import get_experiment
from src.parameterOptimization import PolicyParameterProblem, SinglePointCrossover
from jmetal.util.termination_criterion import StoppingByEvaluations
from jmetal.util.solution import (
    get_non_dominated_solutions,
    print_function_values_to_file,
    print_variables_to_file,
)
from jmetal.util.evaluator import MultiprocessEvaluator, SequentialEvaluator
from jmetal.util.aggregative_function import Tschebycheff
from jmetal.lab.visualization import Plot
from multiprocessing import cpu_count
from jmetal.util.observer import (
    BasicObserver,
    WriteFrontToFileObserver,
    ProgressBarObserver,
)
from src.parameterOptimization.neighborhoodIntegerMutation import (
    IntegerNeighborhoodMutation,
)
from src.parameterOptimization.writeParamsToFile import WriteParamsToFileObserver
from src.types import ExperimentSpec


def front_to_files(front, spec: ExperimentSpec, current_iteration: int):
    # save to files
    print_function_values_to_file(
        front,
        spec.get_learning_path(
            f"FUN.{spec.algorithm.upper()}.PARAMS", current_iteration
        ),
    )
    print_variables_to_file(
        front,
        spec.get_learning_path(
            f"VAR.{spec.algorithm.upper()}.PARAMS", current_iteration
        ),
    )


def plot_front_to_file(front, spec: ExperimentSpec, current_iteration: int):
    plot_front = Plot(
        title="Pareto front approximation", axis_labels=["infection", "cost"]
    )
    plot_front.plot(
        front,
        label=f"{spec.algorithm.upper()}-PARAMS",
        filename=spec.get_learning_path(
            f"{spec.algorithm.upper()}-PARAMS", current_iteration
        ),
        format="png",
    )


def run_experiment_iteration(experiment: ExperimentSpec, iteration: int, cpuCores: int):
    logging.warning(f"[CPU] running on {cpuCores} cores.")

    problem = PolicyParameterProblem(experiment)
    evaluator = (
        SequentialEvaluator()
        if experiment.algorithm == "moead"
        else MultiprocessEvaluator(cpuCores)
    )  # Uses Pool, since ThreadPool does not get past GIL...

    if experiment.algorithm == "nsga2":
        algorithm = NSGAII(
            problem=problem,
            population_size=experiment.population_size,
            offspring_population_size=experiment.population_size,
            mutation=IntegerNeighborhoodMutation(probability=0.3),
            crossover=SinglePointCrossover(probability=1.0),
            termination_criterion=StoppingByEvaluations(
                max_evaluations=experiment.get_num_evaluations()
            ),
            population_evaluator=evaluator,  # Uses Pool, since ThreadPool does not get past GIL...
        )
    elif experiment.algorithm == "nsga3":
        algorithm = NSGAIII(
            UniformReferenceDirectionFactory(
                n_dim=problem.number_of_objectives, n_points=12
            ),
            problem=problem,
            population_size=experiment.population_size,
            mutation=IntegerNeighborhoodMutation(probability=0.3),
            crossover=SinglePointCrossover(probability=1.0),
            termination_criterion=StoppingByEvaluations(
                max_evaluations=experiment.get_num_evaluations()
            ),
            population_evaluator=evaluator,  # Uses Pool, since ThreadPool does not get past GIL...
        )
    elif experiment.algorithm == "moead":
        if problem.number_of_objectives == 3:
            os.makedirs("moead", exist_ok=True)
            vectors = ""
            grid_size = round(sqrt(experiment.population_size))
            for i in range(0, grid_size):
                for j in range(0, grid_size):
                    v = 1.0 * i / (grid_size - 1)
                    w = 1.0 * j / (grid_size - 1)
                    vectors += f"{v} {1-v} {w}\n"
            with open(f"moead/W3D_{experiment.population_size}.dat", "w") as file:
                file.write(vectors)

        algorithm = MOEAD(
            problem=problem,
            population_size=experiment.population_size,
            aggregative_function=Tschebycheff(dimension=problem.number_of_objectives),
            neighbourhood_selection_probability=0.9,
            neighbor_size=20,
            max_number_of_replaced_solutions=2,
            weight_files_path="moead",
            mutation=IntegerNeighborhoodMutation(probability=0.3),
            crossover=SinglePointCrossover(num_parents=3, probability=1.0),
            termination_criterion=StoppingByEvaluations(
                max_evaluations=experiment.get_num_evaluations()
            ),
            population_evaluator=evaluator,  # Uses Pool, since ThreadPool does not get past GIL...
        )
    else:
        raise Exception("Please pick an implemented EMO algorithm...")

    to_file_observer = WriteFrontToFileObserver(
        experiment.get_learning_front_folder(iteration)
    )
    algorithm.observable.register(observer=to_file_observer)

    var_to_file_observer = WriteParamsToFileObserver(
        experiment.get_learning_front_folder(iteration)
    )
    algorithm.observable.register(observer=var_to_file_observer)

    # TODO: Not thread safe... Ideas?
    # plot_observer = PlotFrontToFileObserver(spec.get_learning_plots_folder(), step=50)
    # algorithm.observable.register(observer=plot_observer)

    basic = BasicObserver(frequency=1.0)
    algorithm.observable.register(observer=basic)

    progress_bar = ProgressBarObserver(experiment.get_num_evaluations())
    algorithm.observable.register(observer=progress_bar)

    algorithm.run()
    front = get_non_dominated_solutions(algorithm.get_result())

    front_to_files(front, experiment, iteration)
    plot_front_to_file(front, experiment, iteration)

    try:
        # I would prefer to use pool.join(), but that just throws an exception.
        # TODO: Submit an issue to jmetalpy to do proper cleanup.
        evaluator.pool.close()
    except ValueError as err:
        logging.error(err)
    except AttributeError as err:
        logging.error(err)


def run_experiment(exp: ExperimentSpec, iterationIdx: int, cpuCores: int):
    logging.warning(f"Starting experiment iteration {iterationIdx}")
    run_experiment_iteration(exp, iterationIdx, cpuCores)


if __name__ == "__main__":
    experimentList = sys.argv[1]
    experimentIdx = int(sys.argv[2])
    iterationIdx = int(sys.argv[3])
    cpuCores = min(int(sys.argv[4]), cpu_count())

    logging.warning(f"Run experiment {experimentIdx} from list {experimentList}...")

    run_experiment(
        get_experiment(experimentList, experimentIdx), iterationIdx, cpuCores
    )
