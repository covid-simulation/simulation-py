from typing import List, Tuple
import random
from jmetal.core.solution import IntegerSolution
from jmetal.operator.crossover import Crossover
import copy
import logging

logger = logging.getLogger("CROSSOVER")
logging.basicConfig(level=logging.INFO)


class SinglePointCrossover(Crossover[IntegerSolution, IntegerSolution]):
    def __init__(self, num_parents=2, probability=1.0):
        super(SinglePointCrossover, self).__init__(probability=probability)
        if num_parents < 2 or num_parents > 3:
            raise Exception("Wrong number of parents. Use 2 or 3.")
        self.num_parents = num_parents

    def execute(self, parents: List[IntegerSolution]) -> List[IntegerSolution]:
        if len(parents) != self.num_parents:
            raise Exception(
                "The number of parents is not {}: {}".format(
                    self.num_parents, len(parents)
                )
            )

        if len(parents[0].variables) == 1:
            return parents

        if len(parents) == 2:
            c1, c2 = self.cross(parents[0], parents[1])
            return [c1, c2]

        if len(parents) == 3:
            c1, c2 = self.cross(parents[0], parents[1])
            c3, c4 = self.cross(parents[0], parents[2])
            c5, c6 = self.cross(parents[1], parents[2])
            return [c1, c2, c3, c4, c5, c6]

        return []

    def cross(
        self, p1: IntegerSolution, p2: IntegerSolution
    ) -> Tuple[IntegerSolution, IntegerSolution]:
        crossover_point = random.randrange(1, len(p1.variables))

        child1 = copy.deepcopy(p1)
        child1.variables[crossover_point:] = p2.variables[crossover_point:]

        child2 = copy.deepcopy(p2)
        child2.variables[crossover_point:] = p1.variables[crossover_point:]

        return child1, child2

    def get_number_of_parents(self) -> int:
        return self.num_parents

    def get_number_of_children(self) -> int:
        return (
            2 if self.num_parents == 2 else self.num_parents * 2
        )  # esp. 6 children for 3 parents

    def get_name(self):
        return "Single point crossover"
