from jmetal.core.operator import Mutation
from jmetal.core.solution import IntegerSolution
import random
import logging

logger = logging.getLogger("MUTATION")
logging.basicConfig(level=logging.INFO)


class IntegerNeighborhoodMutation(Mutation[IntegerSolution]):
    def __init__(self, probability=0.2, N=[-5, -4, -3, -2, -1, 1, 2, 3, 4, 5]):
        super(IntegerNeighborhoodMutation, self).__init__(probability=probability)
        self.N = N
        self.probability = probability

    def execute(self, solution: IntegerSolution) -> IntegerSolution:
        idx_to_re_roll = random.randrange(0, len(solution.variables))

        if random.random() < self.probability:
            solution.variables[idx_to_re_roll] = random.randrange(
                solution.lower_bound[idx_to_re_roll],
                solution.upper_bound[idx_to_re_roll],
            )
        else:
            offset = random.choice(self.N)
            solution.variables[idx_to_re_roll] = max(
                min(
                    solution.variables[idx_to_re_roll] + offset,
                    solution.upper_bound[idx_to_re_roll],
                ),
                solution.lower_bound[idx_to_re_roll],
            )

        return solution

    def get_name(self):
        return "Integer neighborhood mutation"
