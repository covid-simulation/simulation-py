from jmetal.core.observer import Observer
from jmetal.util.solution import print_variables_to_file

from pathlib import Path
import os
import logging

LOGGER = logging.getLogger("jmetal")


# Derived from WriteFrontToFileObserver
class WriteParamsToFileObserver(Observer):
    def __init__(self, output_directory: str) -> None:
        """Write function values of the front into files.

        :param output_directory: Output directory. Each front will be saved on a file `FUN.x`."""
        self.counter = 0
        self.directory = output_directory

        if Path(self.directory).is_dir():
            LOGGER.warning(
                "Directory {} exists. Removing contents.".format(self.directory)
            )
            for file in os.listdir(self.directory):
                os.remove("{0}/{1}".format(self.directory, file))
        else:
            LOGGER.warning(
                "Directory {} does not exist. Creating it.".format(self.directory)
            )
            Path(self.directory).mkdir(parents=True)

    def update(self, *args, **kwargs):
        solutions = kwargs["SOLUTIONS"]

        if solutions:
            print_variables_to_file(
                solutions, "{}/VAR.{}".format(self.directory, self.counter)
            )
            self.counter += 1
