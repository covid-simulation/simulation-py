from jmetal.core.operator import Mutation
from jmetal.core.solution import IntegerSolution
import random
import logging

logger = logging.getLogger("MUTATION")
logging.basicConfig(level=logging.INFO)


class IntegerReRollMutation(Mutation[IntegerSolution]):
    def __init__(self, probability=0.2):
        super(IntegerReRollMutation, self).__init__(probability=probability)

    def execute(self, solution: IntegerSolution) -> IntegerSolution:
        idx_to_re_roll = random.randrange(0, len(solution.variables))
        solution.variables[idx_to_re_roll] = random.randrange(
            solution.lower_bound[idx_to_re_roll], solution.upper_bound[idx_to_re_roll]
        )
        return solution

    def get_name(self):
        return "Integer re-roll mutation"
