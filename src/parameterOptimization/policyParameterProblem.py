from jmetal.core.problem import IntegerProblem
from jmetal.core.solution import IntegerSolution

from src.constants import (
    MIN_LECTURE_ATTENDEES,
    MIN_SPORT_ATTENDEES,
)
import logging

from src.experiments.experimentListStage_1 import get_experiment
from src.simulation.wrappers import (
    evaluate_fitness_median,
    evaluate_fitness_median_parallel,
)
from src.types import ExperimentSpec

logger = logging.getLogger("LEARNING")
logging.basicConfig(level=logging.INFO)


class PolicyParameterProblem(IntegerProblem):
    spec = get_experiment(0)

    def __init__(self, spec: ExperimentSpec):
        super(PolicyParameterProblem, self).__init__()

        # [close lecture (with prof) if strictly larger, max people to let into food places, close sport courses like lectures]
        self.lower_bound = [MIN_LECTURE_ATTENDEES, 0, MIN_SPORT_ATTENDEES - 1]
        self.upper_bound = [
            spec.graph.get_max_lecture_attendees_adjusted() + 1,
            500,  # TODO figure this out from the graph
            spec.graph.get_max_sport_attendees_adjusted(),
        ]
        self.number_of_objectives = 3 if spec.objectives == "both" else 2
        self.number_of_variables = 3
        self.number_of_constraints = 0

        self.spec = spec

        # Peak, cumulative, cost
        self.obj_directions = [self.MINIMIZE, self.MINIMIZE, self.MINIMIZE]
        self.obj_labels = (
            ["peak", "total infections", "cost"]
            if spec.objectives == "both"
            else [spec.objectives, "cost"]
        )

    def evaluate(self, solution: IntegerSolution) -> IntegerSolution:
        # logger.warning(f"Evaluate solution {solution.variables}")

        eval_func = (
            evaluate_fitness_median_parallel
            if self.spec.algorithm == "moead"
            else evaluate_fitness_median
        )

        peak, cumulative, cost = eval_func(self.spec.iteration_per_fitness_evaluation)(
            self.spec.graph,
            solution.variables[0],
            solution.variables[1],
            solution.variables[2],
        )

        if self.spec.objectives == "both":
            solution.objectives[0] = peak
            solution.objectives[1] = cumulative
            solution.objectives[2] = cost
        elif self.spec.objectives == "peak":
            solution.objectives[0] = peak
            solution.objectives[1] = cost
        elif self.spec.objectives == "cumulative":
            solution.objectives[0] = cumulative
            solution.objectives[1] = cost

        # logger.warning(
        #    f"Evaluated solution {solution.variables} with fitness {solution.objectives}"
        # )

        return solution

    def get_name(self) -> str:
        return "Policy Parameter Problem"
