import csv
import os
import random
import shutil
from typing import List, Dict, Union

import logging

from src.constants import (
    RAW_FOLDER,
    NAMES_FILE,
    DAYS,
    BLOCKS,
    LECTURE_INFECTION_RATE,
    MIN_LECTURE_ATTENDEES,
    SPORT_BLOCKS,
    MIN_SPORT_ATTENDEES,
    SPORT_INFECTION_RATE,
    FOOD_DAYS,
    FOOD_BLOCKS,
    FOOD_INFECTION_RATE,
    MIN_FLAT_SIZE,
    MAX_FLAT_SIZE,
    FLAT_INFECTION_RATE,
)
from src.graphgen.graphstats import make_graph_stats
from src.graphgen.internalHelpers import internal_load_graph

from src.types import GraphSpec

logger = logging.getLogger("GRAPHGEN")


def load_graph(spec: GraphSpec):
    # Check if the graph exists
    if not os.path.exists(spec.get_people_path()):
        create_graph_csv(spec)

    return internal_load_graph(spec)


def create_graph_csv(spec: GraphSpec):
    """Writes the csv files corresponding to the graph to a graphs/{seed} folder."""
    logger.info(f"Generating graph for spec: {spec}")

    random.seed(spec.seed)

    # Setting up output folder
    output_folder = spec.get_path()
    shutil.rmtree(
        output_folder, ignore_errors=True
    )  # ignoring to avoid blowup if folder does not exist
    os.makedirs(output_folder, exist_ok=True)

    # Loading the raw data
    names: List[str]
    with open(f"./{RAW_FOLDER}/{NAMES_FILE}") as f:
        names = list(map(lambda n: str(n), map(lambda n: n.strip(), f.readlines())))

    students = names[: spec.get_num_students_adjusted()]
    lecturers = names[
        spec.get_num_students_adjusted() : spec.get_num_lecturers_adjusted()
        + spec.get_num_students_adjusted()
    ]
    names = names[
        spec.get_num_students_adjusted()
        + spec.get_num_lecturers_adjusted() : round(len(names) * spec.graph_scaling)
    ]
    food_places = range(spec.get_num_food_places_adjusted())
    courses = range(spec.get_num_coursess_adjusted())
    sports = range(spec.get_num_sports_adjusted())

    # Setting up the data structures for the final csvs
    locations: List[Dict[str, Union[str, int]]] = []
    visits: List[Dict[str, Union[str, int, float]]] = []
    people: List[Dict[str, str]] = []

    for n in names:
        people.append(
            {
                "name": n,
                "state": "S",
                "type": "staff",
                "symptoms": str(False),
                "tested": str(False),
            }
        )
    for n in students:
        people.append(
            {
                "name": n,
                "state": "S",
                "type": "student",
                "symptoms": str(False),
                "tested": str(False),
            }
        )
    for n in lecturers:
        people.append(
            {
                "name": n,
                "state": "S",
                "type": "lecturer",
                "symptoms": str(False),
                "tested": str(False),
            }
        )

    for loc in food_places:
        locations.append({"name": f"food-place-{loc}", "type": "food-place"})

    ########
    # VISITS (and lectures)
    ########
    # Lectures
    for c in courses:
        d = random.choice(DAYS[:-2])
        b = random.choice(BLOCKS[:-1])
        lecturer = random.choice(lecturers)
        visits.append(
            {
                "person": lecturer,
                "location": f"lecture-{c}",
                "day": d,
                "block": b,
                "cost": 5,
                "inverse_infection_rate": 1.0 - LECTURE_INFECTION_RATE,
            }
        )
        lecture_size = random.randrange(
            MIN_LECTURE_ATTENDEES, spec.get_max_lecture_attendees_adjusted() + 1
        )
        for student in random.sample(students, lecture_size):
            visits.append(
                {
                    "person": student,
                    "location": f"lecture-{c}",
                    "day": d,
                    "block": b,
                    "cost": 2,
                    "inverse_infection_rate": 1.0 - LECTURE_INFECTION_RATE,
                }
            )

        locations.append(
            {"name": f"lecture-{c}", "type": "lecture", "degree": lecture_size + 1}
        )

    # Sport courses
    for c in sports:
        d = random.choice(DAYS)
        b = random.choice(SPORT_BLOCKS)
        sport_size = random.randrange(
            MIN_SPORT_ATTENDEES, spec.get_max_sport_attendees_adjusted() + 1
        )
        for p in random.sample(students + lecturers + names, sport_size):
            visits.append(
                {
                    "person": p,
                    "location": f"sport-{c}",
                    "day": d,
                    "block": b,
                    "cost": 1,
                    "inverse_infection_rate": 1.0 - SPORT_INFECTION_RATE,
                }
            )

        locations.append({"name": f"sport-{c}", "type": "sport", "degree": sport_size})

    # Eating
    for f in food_places:
        for n in students + lecturers:
            visits.append(
                {
                    "person": n,
                    "location": f"food-place-{f}",
                    "day": random.choice(FOOD_DAYS),  # no food on weekends
                    "block": random.choice(FOOD_BLOCKS),
                    "cost": 1,
                    "inverse_infection_rate": 1.0 - FOOD_INFECTION_RATE,
                }
            )

    # Working at food places
    for n in names:
        place = random.choice(food_places)
        for d in FOOD_DAYS:
            for b in FOOD_BLOCKS:
                visits.append(
                    {
                        "person": n,
                        "location": f"food-place-{place}",
                        "day": d,
                        "block": b,
                        "cost": 5,
                        "inverse_infection_rate": 1.0 - FOOD_INFECTION_RATE,
                    }
                )

    # Flats
    flat_student_counter = 0
    flat_student_offset = random.randrange(MIN_FLAT_SIZE, MAX_FLAT_SIZE + 1)
    while flat_student_counter + flat_student_offset < len(students):
        flat_name = f"flat-{flat_student_counter}"
        locations.append({"name": flat_name, "type": "flat"})
        for student in students[
            flat_student_counter : flat_student_counter + flat_student_offset
        ]:
            for d in DAYS:
                visits.append(
                    {
                        "person": student,
                        "location": flat_name,
                        "day": d,
                        "block": BLOCKS[-1],
                        "cost": 5000,
                        "inverse_infection_rate": 1.0 - FLAT_INFECTION_RATE,
                    }
                )
        flat_student_counter += flat_student_offset
        flat_student_offset = random.randrange(MIN_FLAT_SIZE, MAX_FLAT_SIZE + 1)
        # The last 2 to 5 students are ignored for the sake of simplicity

    ###############
    # OUTPUT TO CSV
    ###############
    with open(spec.get_people_path(), "w") as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=people[0].keys())
        writer.writeheader()
        for data in people:
            writer.writerow(data)

    with open(spec.get_location_path(), "w") as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=["name", "type", "degree"])
        writer.writeheader()
        for data in locations:
            writer.writerow(data)

    with open(spec.get_visits_path(), "w") as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=visits[0].keys())
        writer.writeheader()
        for data in visits:
            writer.writerow(data)

    #######
    # STATS
    #######
    make_graph_stats(spec)
