import logging
from src.graphgen.helpers import create_graph_csv

from src.types import GraphSpec

logging.basicConfig()
logger = logging.getLogger("GRAPHGEN")
logger.setLevel(logging.INFO)

if __name__ == "__main__":
    sizes = [0.15, 0.5, 1.0]
    caps = [0.5, 0.75, 1.0]

    for i in range(3):
        for s in range(len(sizes)):
            create_graph_csv(
                GraphSpec(seed=i, cap_scaling=caps[s], graph_scaling=sizes[s])
            )
