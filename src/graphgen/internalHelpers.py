import pandas as pd
from src.types import GraphSpec


def internal_load_graph(spec: GraphSpec):
    people = pd.read_csv(
        spec.get_people_path(),
        dtype={
            "name": str,
            "state": str,
            "type": str,
            "symptoms": bool,
            "tested": bool,
        },
        index_col="name",
    )
    locations = pd.read_csv(
        spec.get_location_path(),
        dtype={"name": str, "type": str, "degree": "Int64"},  # the later is nullable
        index_col="name",
    )
    visits = pd.read_csv(
        spec.get_visits_path(),
        dtype={
            "person": str,
            "location": str,
            "day": str,
            "block": str,
            "cost": int,
            "inverse_infection_rate": float,
        },
        index_col="person",  # Note that index != unique.
    )
    return people.sort_index(), locations.sort_index(), visits.sort_index()
