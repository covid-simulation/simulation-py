from src.graphgen.internalHelpers import internal_load_graph
import pandas as pd
import logging

from src.types import GraphSpec

logger = logging.getLogger(__name__)


def make_graph_stats(spec: GraphSpec):
    people, locations, visits = internal_load_graph(spec)
    stats: str = ""

    stats += f"People: {len(people)}\n"
    stats += f"Locations: {len(locations)}\n"
    stats += f"Visits: {len(visits)}\n"

    ##############
    # People stats
    ##############
    stats = make_people_stats(people, stats, visits, "People")
    stats = make_people_stats(
        people[people["type"] == "student"], stats, visits, "Students"
    )
    stats = make_people_stats(
        people[people["type"] == "lecturer"], stats, visits, "Lecturers"
    )
    stats = make_people_stats(people[people["type"] == "staff"], stats, visits, "Staff")

    ################
    # Location Stats
    ################
    stats = make_location_stats(locations, stats, visits, "Locations")
    stats = make_location_stats(
        locations[locations["type"] == "lecture"], stats, visits, "Lectures"
    )
    stats = make_location_stats(
        locations[locations["type"] == "food-place"], stats, visits, "Food Place"
    )
    stats = make_location_stats(
        locations[locations["type"] == "flat"], stats, visits, "Flats"
    )
    stats = make_location_stats(
        locations[locations["type"] == "sport"], stats, visits, "Sport"
    )

    ######################
    # Write to output file
    ######################
    with open(spec.get_stats_path(), "w") as file:
        file.write(stats)


def make_location_stats(locations, stats, visits, name):
    location_visits = pd.merge(locations, visits, left_index=True, right_on="location")
    location_counts = location_visits.groupby("location").count()
    location_counts = location_counts.reset_index()
    stats += f"\n{name} stats:\n"
    stats += f"\tMax Degree {location_counts['type'].max()}\n"
    stats += f"\tMin Degree {location_counts['type'].min()}\n"
    stats += f"\tAvrg. Degree {location_counts['type'].mean()}\n"
    stats += f"\tMedian Degree {location_counts['type'].median()}\n"
    stats += f"\tSum Degree {location_counts['type'].sum()}\n"
    return stats


def make_people_stats(people, stats, visits, name):
    people_visits = pd.merge(people, visits, left_index=True, right_index=True)
    people_counts = people_visits.reset_index().groupby("index").count()
    people_counts = people_counts.reset_index()
    stats += f"\n{name} stats:\n"
    stats += f"\tMax Degree {people_counts['state'].max()}\n"
    stats += f"\tMin Degree {people_counts['state'].min()}\n"
    stats += f"\tAvrg. Degree {people_counts['state'].mean()}\n"
    stats += f"\tMedian Degree {people_counts['state'].median()}\n"
    stats += f"\tSum Degree {people_counts['state'].sum()}\n"
    return stats
