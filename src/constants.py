from typing import List

RAW_FOLDER = "raw_data"
NAMES_FILE = "names.txt"

NUM_COURSES = 2337
NUM_STUDENTS = 7500
NUM_LECTURERS = 1500
MIN_LECTURE_ATTENDEES = 5
MAX_LECTURE_ATTENDEES = 100
NUM_SPORTS = 75
MIN_SPORT_ATTENDEES = 5
MAX_SPORT_ATTENDEES = 40
MIN_FLAT_SIZE = 2
MAX_FLAT_SIZE = 5
NUM_FOOD_PLACES = 4

DAYS: List[str] = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
BLOCKS: List[str] = ["7", "9", "11", "13", "15", "17", "19"]


def get_block_index(block: str) -> int:
    return BLOCKS.index(block)


FOOD_DAYS = DAYS[:-2]
FOOD_BLOCKS = BLOCKS[1:-2]

SPORT_BLOCKS = BLOCKS[4:]  # From 1500 onward all days

INITIAL_INFECTIONS = 25
MAX_WEEKS = 27

EXPOSED_TO_INFECTED_CHANCE = 0.192
# According to other paper 0.0037*removal rate.
# Removal rate is 1/5 days.
INFECTED_TO_DEAD_CHANCE = 0.00074
# removal rate - death rate
INFECTED_TO_RECOVERED_CHANCE = 0.19926
GLOBAL_INFECTION_RATE = 0.5
LECTURE_INFECTION_RATE = 0.5
FOOD_INFECTION_RATE = 0.4
SPORT_INFECTION_RATE = 0.7
FLAT_INFECTION_RATE = 0.5
# used for global infections
RANDOM_MEETING_RATE = 0.05
# More or less a guess
SYMPTOM_RATE = 0.5

COST_QUARANTINED_FLAT = 0
COST_QUARANTINED_LECTURE = 0
