import sys
from typing import List

from pymoo.factory import get_performance_indicator
import numpy as np
from pathlib import Path
import logging

logger = logging.getLogger("HYPERVOLUME")
logging.basicConfig(level=logging.INFO)

MAX_PEOPLE = 5000  # Used for cumulative infections and peak
MAX_COST = 25000  # Guesswork for 5000 people


def read_solutions(filename: str) -> List[List[float]]:
    """Reads a reference front from a file.
    :param filename: File path where the front is located.
    """
    points = []

    if Path(filename).is_file():
        with open(filename) as file:
            for line in file:
                vector = [float(x) for x in line.split()]

                points.append(vector)
    else:
        logger.warning("Reference front file was not found at {}".format(filename))

    return points


def normalize_points(pts: List[List[float]]) -> List[List[float]]:
    if len(pts[0]) == 2:
        return list(map(lambda p: [p[0] / MAX_PEOPLE, p[1] / MAX_COST], pts))
    elif len(pts[0]) == 3:
        return list(
            map(lambda p: [p[0] / MAX_PEOPLE, p[1] / MAX_PEOPLE, p[2] / MAX_COST], pts)
        )
    else:
        logger.error(f"Not able to handle {len(pts[0])} dimensional points!")
        return []


if __name__ == "__main__":
    # Dominated points are correctly ignored
    fronts = [(np.array(normalize_points(read_solutions(f))), f) for f in sys.argv[1:]]

    hv = get_performance_indicator("hv", ref_point=np.array([1, 1, 1]))
    results = list(map(lambda f: (hv.calc(f[0]), f[1]), fronts))

    print(results)

    with open(f"hv/results.csv", "w") as file:
        file.write("\n".join(map(lambda r: f"{r[0]}\t{r[1]}", results)))
