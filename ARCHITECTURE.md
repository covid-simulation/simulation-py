# Modules and code organization
This repository contains an abstract simulation of an epidemic disease at university.
While the actual implementation focuses on this example for now, different scenarios can be easily constructed by changing graph generation accordingly.

Currently, there are four stages in working with the project.
First one has to generate a graph by running the *graphgen* module (e.g. ```poetry run python -m src.graphgen```).
This creates the graph that is later used by both the simulation itself and transitively by the learning algorithm.
Note that across the project **the random seed is used as a key for the graph**.
This means a graph is both generated with a fixed, reproducible seed and later uses that seed as a unique identifier.

After generating a graph the *simulation* module can be run for a specific number of iterations to get statistically reliable data.
The *parameterOptimization* module takes a set of policy parameters and optimizes them via NSGA-II.
The data gathered during a multi-iteration run of the simulation can then be analysed with the *stats* module.

Note that all constants can be found [in the constants.py file](/src/constants.py). 
Using them both graph generation and simulation can be tweaked.
If you find a hard-coded number somewhere other than this file, you can poke me until I fix it.

# graphgen

The generation of a new graph is currently just one big function in the module's [main file](/src/graphgen/__main__.py).
It first prepares constants and data structures, then iteratively generates the graph. 
In the end the graph is written to csv in the graphs/{seed} folder.

After that the graph is immediately read in again and basic statistics (degrees for different types of people/location) are computed.
Those are saved to a *graph-stats.txt* file beside the graph data.

## Graph data

The data itself is basically three [pandas dataframes](https://pandas.pydata.org/pandas-docs/stable/user_guide/dsintro.html) after reading the csv files back in.
There is the *persons*, which is the only dataframe that is actually modified, since it contains the current infection status for each simulated individual.
*Locations* form the other node set of the bipartite graph we model.
Lastly, the *visits* represent the edges between them.

Note that pandas is used instead of a more graph oriented framework, since it is very difficult to say from the outside (and rather unlikely as well) whether a given framework properly recognizes and optimizes for bipartite graphs.
Pandas API, which is more similar to classical SQL as far as queries go, allows one to merge either all three or only two of the tables to only construct the data currently needed.

**For an overview of the fields of the tables [refer to the loading code under /src/simulation/helpers.py](/src/graphgen/helpers.py)**.
It lists all fields with their respective datatypes.
Field names are mostly self-explanatory.
Note, however, that the *tested* attribute on people is currently unused.
An explanation for the *inverse_infection_rate* can be found in the explanation of the simulation module.

# simulation

This module is the heart of the project.
Direct module execution [via the main file](/src/simulation/__main__.py) can be helpful for testing and profiling aside from running a set of iterations to get dependable statistical data.
Any starting of a simulation will likely happen from the convenience functions [in the wrappers.py file](/src/simulation/wrappers.py).

The simulation itself ([in the simulation.py file](/src/simulation/simulation.py)) iterates over the days of the week for as many weeks as needed (either a maximum from the constants or no more infections).
Initially a random subset of the people are infected. 
The rough control flow for a single day is like this:
* Exposed people -> infected with a certain probability
* Infection
* Global Infection
* Infected to dead
* Infected to recovered
* Gathering stats for the timestep

## Infection types and implementation
Infection ([in the infection.py file](/src/simulation/infection.py)) is the code where the simulation spends most of its time.
Global infection is only a very small part of that. 
It calculates the expected infections depending on a random global meeting rate, and a respective infections rate.
Then it samples those newly infected from the uninfected population.

The local infection, which takes into account our graph structure, has roughly these steps:
* Getting currently *active visits* for the current day and time-block under policies and quarantine. Especially, this is where the optimization parameters have an effect.
* Group infected by location and aggregate the total infection chance for visiting a given location.
* Get uninfected and for each location visited roll if an infection occurs.

These steps are performed for each time-block of the given day.
Note that costs of canceled visits are tracked throughout the code and summed up.
  
The rolls for newly infected need a short explanation as well:
In a naive implementation we would roll for every interaction and if the infection chance is met, an infection occurs.
However, that requires a lot of random numbers, and their generation is (somewhat) expensive and unnecessary.
Instead, we look at the *inverse infection rate*, that is 1-infection rate.
For a given location we can then multiply those chances of not-infection to get a total chance of not-infection (without doing error prone calculations for the total infection rate).
Thus, we only need to roll once for every location visited by a person.

# parameterOptimization

This is a bit hacky still and likely to be smoothed out now that the simulation is finished.
Learning is done via [jmetalpy](https://github.com/jMetal/jMetalPy).
In principle we have two objectives: Cumulative infections and policy cost.
The cumulative infections might be replaced with the infection peak - the fitness function returns all three.

We then have three paramteres to tune:
* The maximal number of lecture attendees before it is canceled/digitalized.
* The maximal number of people let into food places per block (randomly sampled from those that want in).
* The maximal size of a sport course, before it is shut down.

All three of these policies are implemented beside the infection routines, their function names starting with *policy*.

# stats
Mostly stubs for now. Goal is to take the csv from the run-stats folder and do statistical processing on it. 
To speed up the simulation only most basic data is gathered there.
Things like peaks, new infections per timestep or cumulative infections can then be calculated afterward. 