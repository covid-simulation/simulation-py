# Experiments to run (on the cluster)

## Which orthogonal choices can we make?
* Size of the graph (1500, 5000, 10_000)
* Objectives: Peak vs. Cumulative vs. Both
* Algorithm: NSGA2 vs. NSGA3 vs. MOEAD
* Fitness Evaluation Strategy: 1 eval vs. 7-median vs 23-median
* Different graphs (can be generated from arbitrary int seeds, overall structure only differs in edges)
=> If we want to look at everything, we get (81 * number of seeds) experiments. That's a bit much.
  
## General Plan
* Stage 1: Focus on one graph for initial tuning (5000 people).
  For this compare algorithms, objectives and simple and 31-median evaluation.
  => 18 experiments
    
* Stage 2: Pick an algorithm from 1st.
  For this run objectives, all evaluations and sizes.
  => 27 experiments (- 6 from stage 1) = 21
  
* Stage 3: Grab a few knee points and run normal iterations (31 or more, then median) to get run statistics for curves and superspreader-locations.
  Maybe check if knee points can we transferred between different graphs.
  => Runs a lot faster, bounded by my ability to sight and interpret results.
  
* Stretch Goal 1: Check how much difference the graph makes.
  Pick a fixed combination of algorithm, objective, size and evaluation.
  Then run a few graph seeds.
  => 6 or 9 to get a nice figure grouping in the thesis ;)
  
## Open Questions
* Do multiple iterations of learning and aggregate fronts in some way to get better results. How to handle 1-eval runs here?