#!/bin/bash
#
#SBATCH --job-name=run_debug_queue
#SBATCH --partition=all
#
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=12
#
#SBATCH --array=0-17

srun python3 -m src.parameterOptimization debug $SLURM_ARRAY_TASK_ID 0 12