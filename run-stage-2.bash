#!/bin/bash
#
#SBATCH --job-name=run_experiments-stage-2
#SBATCH --nodelist=ant6
#
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=120
#
#SBATCH --array=18-20

srun python3 -m src.parameterOptimization stage-2 $SLURM_ARRAY_TASK_ID 0 120